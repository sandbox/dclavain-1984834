<?php

/**
 * @file
 * Shortcut url field file helper.
 */

/**
 * Helper load shortcut url field object.
 */
function shortcut_url_field_load($id) {
  return db_fetch_object(db_query("SELECT * FROM {shortcut_url} WHERE id = %d", $id));
}

/**
 * Helper get vocabulary content type.
 */
function _shortcut_url_field_get_vocabulary($type = NULL) {
  $vocabularies = taxonomy_get_vocabularies($type);

  $voc = array();
  foreach ($vocabularies as $vid => $obj) {
    $voc[$vid] = $obj->name;
  }

  return $voc;
}

/**
 * Update parents when son nodes are update.
 */
function _shortcut_url_field_node_update($nid, $hook = 'nodeapi') {
  $results = db_query("SELECT DISTINCT parent FROM {shortcut_url_history} WHERE nid = %d", $nid);

  $debug = variable_get('shortcut_url_field_settings_debug', 0);

  while ($row = db_fetch_object($results)) {
    $parent = $row->parent;
    $node_parent = node_load($parent);
    node_save($node_parent);

    if ($debug) {
      $hook = ($hook == 'nodeapi') ? 'Node Api' : 'Cron';
      watchdog('shortcut_url_field', "Update node @nid,  Update node parent @parent by @hook",
        array(
          '@nid' => $nid,
          '@parent' => $parent,
          '@hook' => $hook,
        ), WATCHDOG_DEBUG);
    }
  }
}
