<?php
/**
 * @file
 * Module admin.
 */

/**
 * Handler Add/Edit shortcut url form.
 *
 * @param array $form_state
 *   Form state array.
 * @param mixed $shortcut_url_field
 *   $shortcut_url_field Object for edit.
 *
 * @return array
 *   Form.
 */
function shortcut_url_field_add_form($form_state, $shortcut_url_field = NULL) {
  $form = array();

  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#description' => t('This is the unique name of the shortcut url. It must contain only alphanumeric characters and underscores; it is used to identify the view internally and to generate unique names for this shortcut url.'),
    '#access' => empty($shortcut_url_field),
    '#default_value' => $shortcut_url_field->machine_name,
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('You can use a more descriptive name for this shortcut url here. Spaces are allowed'),
    '#default_value' => $shortcut_url_field->name,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $shortcut_url_field->description,
    '#description' => t('Description of the group'),
  );

  $type = !empty($form_state['values']['type']) ? $form_state['values']['type'] : $shortcut_url_field->type;

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_get_types('names'),
    '#default_value' => $type,
    '#description' => t('Select the content type.'),
    '#required' => TRUE,
    '#ahah' => array(
      'path' => 'admin/build/shortcut/ahah-content-type',
      'wrapper' => 'edit-vid-wrapper',
      'effect' => 'fade',
    ),
  );

  $form['vid'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy'),
    '#options' => _shortcut_url_field_get_vocabulary($type),
    '#required' => TRUE,
    '#default_value' => $shortcut_url_field->vid,
  );

  $form['trigger_rule'] = array(
    '#type' => 'textarea',
    '#title' => t('Path rule code'),
    '#description' => '<p>' . t('The variables available to your code are: ') . '<code>$node, $node_dest, and $context</code>' . t('. Code must be return a string value with url value') . '<code> return "/node/" . $node->taxonomy[1256]->description;</code>. ' . '</p><p>' . t("Here's a simple example: ") . '<code>return "http://www.example.com/" . $node->id . "/" . $dest->nid . "/" . $context->id;</code>. ' . '</p>',

    '#required' => TRUE,
    '#default_value' => $shortcut_url_field->trigger_rule,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('shortcut_url_field_validate'),
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function shortcut_url_field_validate($form, &$form_state) {
  if ($form['#parameters'][2]) {
    // Shortcut machine_name is not updatable.
    return;
  }

  $machine_name = $form_state['values']['machine_name'];

  // Shortcut machine_name must be alphanumeric or underscores, no other
  // punctuation.
  if (preg_match('/[^a-zA-Z0-9_]/', $machine_name) || is_numeric($machine_name)) {
    form_error($form['machine_name'], t('Shortcut machine name must be alphanumeric or underscores only, but cannot be numeric.'));
  }

  // Shortcut machine_name must already exist.
  $shortcut = db_result(db_query("SELECT id from {shortcut_url} where machine_name = '%s'", $machine_name));
  if ($shortcut) {
    form_error($form['machine_name'], t('You must use a unique machine name for this shortcut url.'));
  }

}

/**
 * Handle Save/Modify import form.
 */
function shortcut_url_field_add_form_submit($form, &$form_state) {
  if ($form['#parameters'][2]) {
    $form_state['values']['id'] = $form['#parameters'][2]->id;
    drupal_write_record('shortcut_url', $form_state['values'], 'id');
    drupal_set_message(t('Shortcut url @shortcut_url_name has been updated.', array('@shortcut_url_name' => $form_state['values']['name'])));
  }
  else {
    drupal_write_record('shortcut_url', $form_state['values']);
    drupal_set_message(t('Shortcut url @shortcut_url_name has been created.', array('@shortcut_url_name' => $form_state['values']['name'])));
  }
  $form_state['redirect'] = 'admin/build/shortcut';
}

/**
 * Delete shortcut url form.
 */
function shortcut_url_field_delete_form($form_state, $shortcut_url_field = NULL) {
  $form = array();

  return confirm_form($form, t('Are you sure that you want to delete shortcut url @name?',
    array('@name' => $shortcut_url_field->name)), 'admin/build/shortcut');
}

/**
 * Handler delete shortcut url form.
 */
function shortcut_url_field_delete_form_submit($form, &$form_state) {
  if ($form['#parameters'][2]) {
    $id = $form['#parameters'][2]->id;
    db_query("DELETE FROM {shortcut_url} WHERE id = %d", $id);
    drupal_set_message(t('@shortcut_url_field has been deleted.', array('@shortcut_url_field' => $shortcut_url_field->name)));
  }
  $form_state['redirect'] = 'admin/build/shortcut';
}

/**
 * Ahah function.
 */
function _shortcut_url_field_ahah_content_type() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // HACK: Select values changing never get recognized.
  unset ($form['type']['#value']);

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  $checkboxes = $form['vid'];
  $output = drupal_render($checkboxes);

  // Final rendering callback.
  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * List shortcut administration page.
 *
 * @return string
 *   List table of shortcut.
 */
function shortcut_url_field_page() {
  $types = node_get_types('names');
  $vocabularies = _shortcut_url_field_get_vocabulary();

  $header = array(
    t('Machine'),
    t('Name'),
    t('Description'),
    t('Content Type'),
    t('Vocabulary'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $results = db_query("SELECT * FROM {shortcut_url} ORDER BY id ASC");

  $rows = array();
  while ($item = db_fetch_object($results)) {
    $rows[] = array(
      check_plain($item->machine_name),
      check_plain($item->name),
      check_plain($item->description),
      check_plain($types[$item->type]),
      check_plain($vocabularies[$item->vid]),
      l(t('Edit'), 'admin/build/shortcut/' . $item->id . '/edit'),
      l(t('Delete'), 'admin/build/shortcut/' . $item->id . '/delete'),
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Handler settings forms.
 */
function shortcut_url_field_settings_page($form_state) {
  $form = array();

  $form['shortcut_url_field_settings_sync_mod'] = array(
    '#type' => 'radios',
    '#title' => t('Synchronization type'),
    '#description' => t('Define whether the syncronization should be made on each node operation (insert / save node) or using an cron.'),
    '#options' => array(
      'nodeapi' => t('Node operation (insert / save node)'),
      'cron' => t('Cron'),
    ),
    '#default_value' => variable_get('shortcut_url_field_settings_sync_mod', 'nodeapi'),
  );

  $form['shortcut_url_field_settings_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron node process limit'),
    '#description' => t('The number of nodes will be processed with each execution of the cron. 0 for all'),
    '#default_value' => variable_get('shortcut_url_field_settings_cron_limit', 10),
  );

  $form['shortcut_url_field_settings_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('Add debug messages in watchdog'),
    '#default_value' => variable_get('shortcut_url_field_settings_debug', 0),
  );

  return system_settings_form($form);
}
