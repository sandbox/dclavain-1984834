<?php

/**
 * @file
 * This file contains CCK formatter related functionality.
 */

/**
 * Theme function editing form.
 */
function theme_text_shortcut_url($element) {
  return $element['#children'];
}

/**
 * Formatter Default.
 */
function theme_shortcut_url_field_formatter_default($element) {

  $item = $element['#item']['safe']['value'];
  $context = '';
  foreach ($element['#item']['safe']['shortcut_url_values'] as $key => $value) {
    $e = array(
      '#title' => $key,
      '#value' => $value,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $context .= theme('fieldset', $e);
  }

  $fieldset = array(
    '#title' => t('Contexts'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => $context,
  );

  return (empty($context)) ? $item : $item . theme('fieldset', $fieldset);
}
/**
 * Formatter plain function.
 */
function theme_shortcut_url_field_formatter_plain($element) {
  return $element['#item']['value'];
}
