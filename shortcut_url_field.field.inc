<?php

/**
 * @file
 * Definition field and widget.
 */

/**
 * Implements hook_field_info().
 */
function shortcut_url_field_field_info() {
  return array(
    'text_shortcut_url' => array(
      'label' => t('Text shortcut url'),
      'description' => t('Text Area (multiple rows with shortcut url)'),
    ),
  );
}

/**
 * Implements hook_widget_info().
 */
function shortcut_url_field_widget_info() {
  return array(
    'text_shortcut_url' => array(
      'label' => t('Text Area (multiple rows with shortcut url)'),
      'field types' => array('text_shortcut_url'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_DEFAULT,
      ),
    ),
  );
}
/**
 * Implements hook_elements().
 */
function shortcut_url_field_elements() {
  return array(
    'text_shortcut_url' => array(
      '#input' => TRUE,
      '#columns' => array('value','shortcut_values'),
      '#delta' => 0,
      '#process' => array('_shortcut_url_field_process'),
      '#filter_value' => FILTER_FORMAT_DEFAULT,
    ),
  );
}

/**
 * Implements hook_field_settings().
 */
function shortcut_url_field_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      // Formulario setting global para ese campo en todos
      // los TDC que se encuentren.
      break;

    case 'save':
      // Devuelve ese forulario global.
      break;

    case 'database columns':
      $columns['value'] = array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
        'views' => TRUE,
      );
      $columns['shortcut_url_values'] = array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
        'serialize' => TRUE,
        'views' => TRUE,
      );
      return $columns;
  }
}

/**
 * Implements hook_widget_settings().
 */
function shortcut_url_field_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      return _shortcut_url_field_widget_settings_form($widget);

    case 'save':
      return _shortcut_url_field_widget_settings_save();
  }
}

/**
 * Helper hook_widget_settings in form operation.
 */
function _shortcut_url_field_widget_settings_form($widget) {
  $form = array();
  $rows = (isset($widget['rows']) && is_numeric($widget['rows'])) ? $widget['rows'] : 5;
  $default_trigger_rule = (isset($widget['default_trigger_rule'])) ? $widget['default_trigger_rule'] : '\'return /node/\' . $node->nid';
  $form['rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Rows'),
    '#default_value' => $rows,
    '#element_validate' => array('_element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['default_trigger_rule'] = array(
    '#type' => 'textarea',
    '#title' => t('Default path rule code'),
    '#description' => '<p>' . t('The variables available to your code are: ') . '<code>$node, $node_dest, and $context</code>' . t('. Code must be return a string value with url value') . '<code> return "/node/" . $node->taxonomy[1256]->description;</code>. ' . '</p><p>' . t("Here's a simple example: ") . '<code>return "http://www.example.com/" . $node->id . "/" . $dest->nid . "/" . $context->id;</code>. ' . '</p>',
    '#required' => TRUE,
    '#default_value' => $widget['default_trigger_rule'],
  );

  $form['size'] = array('#type' => 'hidden', '#value' => $size);
  return $form;

}

/**
 * Helper hook_widget_settings in save operation.
 */
function _shortcut_url_field_widget_settings_save($widget) {
  return array('rows', 'size', 'default_trigger_rule');
}

/**
 * Implements hook_widget().
 */
function shortcut_url_field_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  return $element;
}

/**
 * Process an individual element.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function _shortcut_url_field_process($element, $edit, $form_state, $form) {
  $field = $form['#field_info'][$element['#field_name']];
  $field_key   = $element['#columns'][0];
  $element[$field_key] = array(
    '#type' => 'textarea',
    '#default_value' => isset($element['#value'][$field_key]) ? $element['#value'][$field_key] : NULL,
    '#rows' => !empty($field['widget']['rows']) ? $field['widget']['rows'] : 10,
    '#weight' => 0,
    // The following values were set by the content module and need
    // to be passed down to the nested element.
    '#title' => $element['#title'],
    '#description' => $element['#description'],
    '#required' => $element['#required'],
    '#field_name' => $element['#field_name'],
    '#type_name' => $element['#type_name'],
    '#delta' => $element['#delta'],
    '#columns' => $element['#columns'],
  );

  if (!empty($field['text_processing'])) {
    $filter_key  = (count($element['#columns']) == 2) ? $element['#columns'][1] : 'format';
    $format = isset($element['#value'][$filter_key]) ? $element['#value'][$filter_key] : FILTER_FORMAT_DEFAULT;
    $parents = array_merge($element['#parents'], array($filter_key));
    $element[$filter_key] = filter_form($format, 1, $parents);
  }

  // Used so that hook_field('validate') knows where to flag an error.
  $element['_error_element'] = array(
    '#type' => 'value',
    '#value' => implode('][', array_merge($element['#parents'], array($field_key))),
  );

  return $element;
}

/**
 * Implements hook_content_is_empty().
 */
function shortcut_url_field_content_is_empty($item, $field) {
  if (empty($item['value']) && (string) $item['value'] !== '0') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field().
 */
function shortcut_url_field_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'insert':
    case 'update':
      _shortcut_url_field_field_insert($node, $field, $items, $teaser, $page);
      break;

    case 'sanitize':
      _shortcut_url_field_fiel_sanitize($node, $field, $items, $teaser, $page);
      break;

    case 'load':
      return _shortcut_url_field_field_load($node, $field, $items, $teaser, $page);
  }
}

/**
 * Helper hook_field in sanitize operation.
 */
function _shortcut_url_field_fiel_sanitize(&$node, $field, &$items, $teaser, $page) {
  foreach ($items as $delta => $item) {
    $items[$delta]['safe']['value'] = isset($item['value']) ? check_markup($item['value']) : '';

    foreach ($item['shortcut_url_values'] as $id => $shortcut_value) {
      $items[$delta]['safe']['shortcut_url_values'][$id] = isset($shortcut_value) ? check_markup($shortcut_value) : '';
    }
  }
}

/**
 * Helper hook_field() in insert operation.
 */
function _shortcut_url_field_field_insert(&$node, $field, &$items, $teaser, $page) {
  $vids = array_keys(taxonomy_node_get_terms($node, 'vid'));

  foreach ($items as $delta => $value) {
    $result = db_query("SELECT id, machine_name, trigger_rule FROM {shortcut_url} WHERE type = '%s' and vid in (%s)", $node->type, implode(',', $vids));
    // Default context value.
    $items[$delta]['shortcut_url_values']['context_default'] = _shortcut_url_field_get_value($node, $value['value'], NULL, $field['widget']['default_trigger_rule']);

    while ($row = db_fetch_object($result)) {
      $items[$delta]['shortcut_url_values'][$row->machine_name] = _shortcut_url_field_get_value($node, $value['value'], $row);
    }

    // Save node links history.
    _shortcut_url_field_save_history($node, $field, $items);
  }
}

/**
 * Search internal links in value text.
 *
 * @param string $value
 *   Text value
 *
 * @return array
 *   array ('href' => array(), 'nid' => array()).
 */
function _shortcut_url_field_match_all($value) {
  $node_matches = array();
  preg_match_all('/href="(?P<href>\/?node\/(?P<nid>(\d)+))"/', $value, $node_matches);
  return $node_matches;
}

/**
 * Helper translate url in context.
 */
function _shortcut_url_field_get_value($node, $value, $shortcut_value = NULL, $default_trigger_rule = NULL) {
  $node_matches = _shortcut_url_field_match_all($value);
  $replace      = array();

  foreach ($node_matches['href'] as $key => $ref) {
    $dest = node_load($node_matches['nid'][$key]);
    if (empty($shortcut_value)) {
      // Default context value.
      $replace[$ref] = eval($default_trigger_rule);
    }
    else {
      $context = shortcut_url_field_load($shortcut_value->id);
      $replace[$ref] = eval($context->trigger_rule);
    }
  }

  $pattern = array_keys($replace);
  $replacement = array_values($replace);
  return str_replace($pattern, $replacement, $value);
}

/**
 * Save the historic links that contains the field.
 */
function _shortcut_url_field_save_history($node, $field, $items) {

  // Delete history for this node.
  db_query("DELETE FROM {shortcut_url_history} WHERE parent = %d", $node->nid);
  foreach ($items as $delta => $item) {
    if (!$item['_remove']) {
      $node_matches = _shortcut_url_field_match_all($item['value']);
      foreach ($node_matches['nid'] as $key => $nid) {
        $shortcut_url_history = array(
          'field_name' => $field['field_name'],
          'parent' => $node->nid,
          'delta' => $delta,
          'nid' => $nid,
        );
        // Save history node links from this node.
        drupal_write_record('shortcut_url_history', $shortcut_url_history);
      }
    }
  }
}

/**
 * Helper hook_field() in load operation.
 */
function _shortcut_url_field_field_load(&$node, $field, &$items, $teaser, $page) {
  if (empty($items)) {
    return array();
  }

  foreach ($items as $delta => $item) {
    if (isset($item['shortcut_url_values']) && !empty($item['shortcut_url_values'])) {
      $item['shortcut_url_values'] = unserialize($item['shortcut_url_values']);
    }
    // Temporary fix to unserialize data serialized multiple times.
    // See the FileField issue http://drupal.org/node/402860.
    // And the CCK issue http://drupal.org/node/407446.
    while (!empty($item['shortcut_url_values']) && is_string($item['shortcut_url_values'])) {
      $item['shortcut_url_values'] = unserialize($item['shortcut_url_values']);
    }
    $items[$delta] = $item;
  }

  return array($field['field_name'] => $items);
}

/**
 * Implements hook_field_formatter_info().
 */
function shortcut_url_field_field_formatter_info() {
  $formatter = array(
    'default' => array(
      'label' => t('Default'),
      'field types' => array('text_shortcut_url'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'plain' => array(
      'label' => t('Default'),
      'field types' => array('text_shortcut_url'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
  return $formatter;
}
