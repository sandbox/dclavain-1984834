
-- SUMMARY --

Text field that will provide internal links to nodes processed depending on the
context in which it is applied.

Each context will be related to a vocabulary.

-- REQUIREMENTS --

The following modules are required:

* Content Construction Kit (CCK), https://drupal.org/project/cck


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Create/Edit contexts in Administer >> Site building >> Contexts
* Settings contexts in Administer >> Settings >> Contexts
